import React, { Component } from 'react';
import {render} from 'react-dom';
import Board from './Board';

render(<Board />, document.getElementById('container'));
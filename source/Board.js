import React, { Component } from 'react';
import {render} from 'react-dom';
import BP from './BP';

class Board extends Component {
	constructor(){
		this.state = {
			//eventually I'll pull in the BP data from websocket
			bps:[],
		}
	}
	
	componentDidMount(){
		//here's where the data will be fetched via a Websocket query
	}
	
	render() {
		return (
			<div>
			<button onClick={this.addBP.bind(null,'Dad Cena')} > Add new BP</button>
				<div className="board">
					{ this.state.bps.map(this.eachBP) }
				</div>
			</div>
		);	
	}
}

export default Board;

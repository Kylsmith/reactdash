console.log('ged-aout')
	
Websocket.init('ws://10.1.10.33:81', 'Woodard');



var BP = React.createClass({
	
	getInitialState: function(){
		return {editing: false}
	},

	getBPs: function(){
	},
	
	editName: function(){
		this.setState({editing: true});
	},
	
	remove: function(){
		this.props.deleteBP(this.props.index);
	},
	
	save:function(){
		console.log('new comment:' + this.refs.newText.value);
		console.log(this.props);
		this.props.updateBPName(this.refs.newText.value, this.props.index);
		this.setState({editing: false});
	},
	
	renderForm:function(){
		return (
			<div className="bPContainer">
				<textarea ref="newText" defaultValue={this.props.children}></textarea>
				<button onClick={this.save}>Save</button>
			</div>
		);		
	},
	
	renderNormal:function(){
		return (
			<div className="bPContainer">
				<div><h1>{this.props.children}</h1></div>
				<h3>{this.props.bpCC}</h3>
				<button onClick={this.editName} >Edit BP name</button>
				<button onClick={this.remove}>Remove</button>
				
			</div>
		);	
	},
	
	render: function(){
		if (this.state.editing) {
			return this.renderForm();
		} else {
			return this.renderNormal();
		}
	}

});

var Board = React.createClass({
	
	getInitialState:function(){
		return {
			bps: []
		}
	},
	
	addBP: function(name){
		var array = this.state.bps
		array.push(name);
		this.setState({bps:array})
	},
	
	removeBP: function(index){
		var array = this.state.bps
		array.splice(index,1);
		this.setState({bps: array});
	},
	
	updateBP: function(newText,index){
		var array = this.state.bps
		array[index] = newText;
		this.setState({bps: array});
	},
	
	eachBP: function(text,i){
		return(
			<BP key={i} index={i} deleteBP={this.removeBP} updateBPName={this.updateBP}>
				{text}
			</BP> 
		);
	},
	

	
	render:function(){
		return (
			<div>
			<Search>
				
			</Search>
			<button onClick={this.addBP.bind(null,'Dad Cena')} > Add new BP</button>
				<div className="board">
					{ this.state.bps.map(this.eachBP) }
				</div>
			</div>
			
		);	
	}
	
	
});

var Search = React.createClass({
	
	searchBP: function(){
		
		var searchParam = this.refs.searchText.value
		console.log(searchParam);
		Websocket.query('Woodard', 'Dashboard.SearchBPs', {'SearchParam': searchParam}).done(function(result) {
			console.log(result);
		});

	},
	
	// getInitialState:function(){
	// 	return {
			
	// 	}
	// },
	
	
	render:function(){
		return(
			<div>
			      <button  id="BPEntry_search" type="button" onClick={this.searchBP} >Search</button>
	              <label >BP Name</label>
	              <input  id="BPEntry_search_CardName" name="BPEntry_search_CardName" type="text" placeholder="BP Name" ref="searchText"></input>
			</div>		
			)
	},
});

ReactDOM.render(<Board />, document.getElementById('container'));

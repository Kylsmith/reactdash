var Websocket = {
	open: false,
	_connection: null,
	reqID: 0,
	sessionKey: null,
	queue: [],
	version: 3.1,
	versionDate: 29092015,
	debug: true,
	
	init: function(url, repoId, user, pass, timeout) {
		var app = this;
		if (url && url !== '') {
			// Create the actual websocket connection as an internal variable we can use later
			this._connection = new WebSocket(url);
			if (timeout && timeout > 0) this.timeout = timeout;
			// Set up the handlers for the various states of the websocket
			this.bindEventListeners();
			// If a request was interrupted by restarting the connection, try to send it with the new one
			if (app.wait) {
				console.warn('Websocket found unsent request. Attempting to send now...');
				app.wait();
				app.wait = null;
			}
			// Get a session key but return the promise so it can be tracked externally
			return this.authorize(repoId, user, pass).done(function(data) {
				app.sessionKey = data.Session;
				app.repo = repoId;
				app.url = url;
				app.user = user;
				app.pass = pass;
			});
		} else {
			console.error('Websocket.init() needs parameters 1 (ws://IPADDRESS:PORT) and 2 (the name of the repo) in order to work');
			return false;
		}
	},
	
	isReady: function() {
		// This is how we know if the connection is ready to send over
		return this._connection.readyState === 1 && this.open === true;
	},
	
	bindEventListeners: function() {
		var app = this;
		// Open
		this._connection.addEventListener('open', function() {
			app.open = true;
			console.debug('Websocket Connection: OPEN');
		});

		// Close
		this._connection.addEventListener('close', function() {
			app.open = false;
			console.debug('Websocket Connection: CLOSED');
			// We will try to reconnect in the background
			app.init(app.url, app.repo, app.user, app.pass, app.timeout);
		});

		// Error
		this._connection.addEventListener('error', function(event) {
			console.error('Websocket Error: ' + event);
		});

		// Receiving messages
		this._connection.addEventListener('message', function(event) {
			// Parse the incoming string as JSON (assuming the response is stringified)
			var response = JSON.parse(event.data);
			// Resolve the promise that was used to get the data here
			if (response.ReqID) {
				var queueItem = app.queue[response.ReqID];
				if (queueItem) {
					if (queueItem.resolve) app.queue[response.ReqID].resolve(response);
				}
			}
			if(app.debug) console.debug('Websocket Response [' + response.ReqID + ']', response);
		});
	},
	
	sendRequest: function(request) {
		var app = this; 
		function isPromiseResolved(key) {
			// Checks the queue to see if any are resolved or otherwise removable
			return (app.queue.hasOwnProperty(key) &&
			(typeof app.queue[key].state === 'undefined' ||
			(typeof app.queue[key].state !== 'undefined' && app.queue[key].state() !== 'pending')));
		}
		
		// Clear out any old promises that may be lingering in the queue
		for (var key in this.queue) {
			if (isPromiseResolved(key)) delete this.queue[key]; 
		}
		
		// Create a new promise and add it to the queue
		var promise = $.Deferred();
		var json = JSON.stringify(request);
		this.queue[request.ReqID] = promise;		

		if (this.isReady()) {
			// Send the query out through the websocket
			try {
				if(app.debug) console.debug('Websocket Send [' + request.ReqID + ']', json);
				this._connection.send(json);
			} catch(error) {
				console.error('Websocket Error:', error);
			}
		} else {
			// The websocket is not ready to send, so we'll hook up a listener that will wait until 
			// it's is open to send the request, then remove itself
			//console.warn('WebSocket Send [' + request.ReqID + '] waiting for connection to open...');
			this.wait = function() {
				try {
					if(app.debug) console.debug('Websocket Send [' + request.ReqID + ']', json);
					app._connection.send(json);
					app._connection.removeEventListener('open', app.wait);
				} catch(error) {
					console.error('Websocket Error:', error);
				}
			};
			this._connection.addEventListener('open', app.wait);
		}
		
		return promise;
	},
	
	getBaseQuery: function(type, repo) {
		// Since all requests use this basic format, we'll serve new ones here by autopopulating
		// the request ID and session key
		return {
			Type: type || 'RepoRequest',
			ReqID: this.reqID++,
			Session: this.sessionKey,
			Repo: repo || '',
		}
	},
	
	authorize: function(repoID, user, pass) {
		// Used by this.init() to authorize our connection and receive a session key
		// Once you've used init(), you won't need this function any more
		if (user === null) user = '';
		if (pass === null) pass = '';
		if (repoID !== null) { 
			var base = this.getBaseQuery('Auth', repoID);
			base.User = user;
			base.Pass = pass;
			delete base.Session;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket.authorize requires at least a repoID as its first argument');
			return false;
		}	
	},	
	
	query: function(repoID, queryID, parameters) {
		// Query the database using the predefined query and given parameters
		if (parameters === null || parameters === '') parameters = {};
		if (queryID !== null && typeof parameters === 'object') { 
			var base = this.getBaseQuery('RepoPDQ', repoID);
			base.ID = queryID;
			base.Params = parameters;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket Error:');
			return false;
		}
	},
	
	mail: function(repoID, template, account, params) {
		// Sends an email using the given template through the given SMTP repo
		if (repoID !== null && template !== null) { 
			var base = this.getBaseQuery('SendMailTemplate', repoID);
			base.Template = template;
			base.MailAccount = account;
			base.Params = params;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket Error:');
			return false;
		}
	},
	
	write: function(content, extension) {
		// Sends an XML string to the server containing changes to the database
		// The extension is xml by default, though 'json' is another option 
		if (extension === null || extension === '') extension = 'xml';
		if (content !== null) { 
			var base = this.getBaseQuery('WriteFile');
			base.Content = content;
			base.FileExtension = extension;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket Error:');
			return false;
		}
	},
		
	direct: function(content, async, extension) {
		// Sends an XML string to the server containing changes to the database
		// If async is set to false, the response will come back when the actual database change 
		// has been made. If it's true, you'll get an immediate response that tells you if the 
		// import call worked in general.
		// The extension is xml by default, though 'json' is another option 
		if (extension === null || extension === '') extension = 'xml';
		if (async === null || async === '') async = 'false';
		if (content !== null) { 
			var base = this.getBaseQuery('RepoFileImport');
			base.Content = content;
			base.FileExtension = extension;
			base.Async = async;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket Error:');
			return false;
		}
	},
	
	crystal: function(repoID, reportKey, format) {
		// Requests a crystal report by name, and in the given format. The report
		// is returned in that format, as a base64 string
		if (format === null || format === '') format = 'PortableDocFormat';
		if (repoID !== null && reportKey !== null) {
			var base = this.getBaseQuery('CrystalReportRequest', repoID);
			base.ReportKey = reportKey;
			base.Format = format;
			//console.log(base);
			return this.sendRequest(base);
		} else {
			console.error('Websocket.crystal requires the repoID and reportKey as its first two arguments');
			return false;
		}
	},
	
	enprise: function(repoID, content, jobID) {
		// Writes an enprise job. Supplying jobID will update the given job.
		if (repoID !== null && content !== null) {
			content.Repo    = repoID;
			content.ReqID   = this.reqID++;
			content.Session = this.sessionKey;
			content.Type    = 'SetEnpriseJob';
			if(jobID !== null && jobID !== '') content.JobID = jobID;
			return this.sendRequest(content);
		} else {
			console.error('Websocket.enprise requires the repoID and content as its first two arguments');
			return false;
		}
	},
	
	raw: function(object) {
		// Sends a raw javascript object through the websocket with minimal tampering
		// (Session and ReqID). This should ONLY be used for testing purposes, and not
		// as a replacement for any of the other protocols.
		if (object === null || object === '') {
			console.error('Websocket.raw requires 1 attribute: a javascript object to send through the websocket');
			return false;
		}
		object.ReqID = this.reqID++;
		object.Session = this.sessionKey;
		return this.sendRequest(object);
	},
	
/* 	attachment: function(repoID, options) {
		if (repoID !== null) {
			var base = this.getBaseQuery('SetTSAttach', repoID);
			
		} else {
			
		}
	}, */

};
